#include "importgit.h"
#include "ui_importgit.h"
#include <QMessageBox>
#include <QStyle>
#include <QDesktopWidget>

importGit::importGit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::importGit)
{
    ui->setupUi(this);
    ui->passTxt->setEchoMode(QLineEdit::Password);

    this->setFixedSize(this->size());

    setWindowFlags(Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint);

    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

importGit::~importGit()
{
    delete ui;
}

void importGit::on_openBtn_clicked()
{
    if (ui->linkTxt->text() == "")
    {
        QMessageBox message(this);
        message.setText("Please enter a git repository link");
        message.exec();
    }
    else
    {
        //TODO: Make this import a git project to a temp folder.
    }
}

void importGit::on_cancelBtn_clicked()
{
    close();
    parentWidget()->show();
}
