#include "dependenciescheck.h"
#include "ui_dependenciescheck.h"
#include <QTimer>
#include <QStyle>
#include <QDesktopWidget>

DependenciesCheck::DependenciesCheck(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DependenciesCheck)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window);
    this->setFixedSize(this->size());

    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

void DependenciesCheck::loadCompleted()
{
    QString prog = "dotnet --version";
    QProcess* process = new QProcess(this);
    process->start(prog);
    process->waitForFinished();
    QString tmp = process->readAll();

    if (tmp.isEmpty())
    {
        QMessageBox Msgbox;
        Msgbox.setText(".NET Core doesn't seem to installed. (Is the dotnet command globally accessible?)");
        Msgbox.setIconPixmap(QPixmap("./Resources/error.png"));
        Msgbox.exec();

        QTimer::singleShot(500, this, SLOT(close()));
        std::exit(1);
    }
    else
    {
        QTimer::singleShot(500, this, SLOT(close()));
    }
}

DependenciesCheck::~DependenciesCheck()
{
    delete ui;
}
