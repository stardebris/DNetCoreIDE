#include "startup.h"
#include "ui_startup.h"
#include <QProcess>
#include <QMessageBox>
#include <QDebug>
#include "importgit.h"
#include "dependenciescheck.h"
#include <QTimer>
#include "get.h"
#include "projectwizard.h"
#include <QFileDialog>
#include <QStyle>
#include <QDesktopWidget>
#include "mainwindow.h"

Startup::Startup(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Startup)
{
    ui->setupUi(this);

    DependenciesCheck *dep = new DependenciesCheck();
    dep->show();

    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

Startup::~Startup()
{
    delete ui;
}

void Startup::on_createNewBtn_clicked()
{
    projectWizard *projwzz = new projectWizard(this);
    projwzz->show();
    this->hide();
}

void Startup::on_closeBtn_clicked()
{
    QCoreApplication::quit();
}

void Startup::on_openGitBtn_clicked()
{
    importGit *git = new importGit(this);
    git->show();
    this->hide();
}

void Startup::on_openBtn_clicked()
{
    QFileDialog *dialog = new QFileDialog(this, "", QDir::homePath(), "Project Files (*.dncproj);;");
    dialog->setFileMode(QFileDialog::ExistingFile);
    dialog->exec();

    if (dialog->selectedUrls().count() > 0 && !dialog->selectedUrls()[0].toString().isEmpty())
    {
        MainWindow *main = new MainWindow(this);
        main->projectFile = dialog->selectedUrls()[0].toString().replace("file://", "");
        main->show();
        this->hide();
    }
}
