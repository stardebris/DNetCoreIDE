#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->toDoListWidget->setVisible(false);
}

void MainWindow::showEvent( QShowEvent* event )
{
    QWidget::showEvent( event );

    //Set all window info such as files, and todo's
    setWindowTitle(".NET Core IDE - " + projectFile);

    project = DNetProject().readFile(projectFile);
    QTreeWidget * tree = ui->projectFiles;

    QTreeWidgetItem * topLevel = new QTreeWidgetItem();
    topLevel->setText(0, "Project Files");

    for (int i = 0; i < this->project.projectFiles.count(); i++)
    {
        QTreeWidgetItem * item = new QTreeWidgetItem();
        item->setText(0, project.projectFiles.at(i));
        topLevel->addChild(item);
    }

    tree->addTopLevelItem(topLevel);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_toDoListWidget_visibilityChanged(bool visible)
{
    ui->todoListToggle->setChecked(visible);
}

void MainWindow::on_projectFilesWidget_visibilityChanged(bool visible)
{
    ui->projectFilesToggle->setChecked(visible);
}

void MainWindow::on_compileOutputWidget_visibilityChanged(bool visible)
{
    ui->cOutputToggle->setChecked(visible);
}

void MainWindow::on_outputWidget_visibilityChanged(bool visible)
{
    ui->outputToggle->setChecked(visible);
}

void MainWindow::on_projectFilesToggle_toggled(bool arg1)
{
    ui->projectFilesWidget->setVisible(arg1);
}

void MainWindow::on_todoListToggle_toggled(bool arg1)
{
    ui->toDoListWidget->setVisible(arg1);
}

void MainWindow::on_cOutputToggle_toggled(bool arg1)
{
    ui->compileOutputWidget->setVisible(arg1);
}

void MainWindow::on_outputToggle_toggled(bool arg1)
{
    ui->outputWidget->setVisible(arg1);
}

void MainWindow::on_projectFiles_doubleClicked(const QModelIndex &index)
{

}
