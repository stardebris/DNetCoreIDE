#-------------------------------------------------
#
# Project created by QtCreator 2016-10-26T22:26:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DNetCoreIDE
TEMPLATE = app


SOURCES += main.cpp\
        startup.cpp \
    mainwindow.cpp \
    importgit.cpp \
    dependenciescheck.cpp \
    get.cpp \
    projectwizard.cpp \
    dnetproject.cpp \
    loading.cpp

HEADERS  += startup.h \
    mainwindow.h \
    importgit.h \
    dependenciescheck.h \
    get.h \
    projectwizard.h \
    dnetproject.h \
    loading.h

FORMS    += startup.ui \
    mainwindow.ui \
    importgit.ui \
    dependenciescheck.ui \
    projectwizard.ui \
    loading.ui
