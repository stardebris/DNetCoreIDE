#ifndef LOADING_H
#define LOADING_H

#include <QWidget>
#include <QDesktopWidget>
#include <QDialog>
#include <QProcess>
#include "get.h"

namespace Ui {
class Loading;
}

class Loading : public QDialog
{
    Q_OBJECT

public:
    explicit Loading(QWidget *parent = 0);
    QString projectName;
    QString projectTargetFramework;
    QString currentPath;
    QString projectLang;
    QString projectType;
    bool makeDirectory;
    bool initGit;
    void loadCompleted();
    ~Loading();

public slots:
        void initProject();

private:
    Ui::Loading *ui;

protected:
    void showEvent(QShowEvent *e)
    {
        QDialog::showEvent(e);
        loadCompleted();
    }
};

#endif // LOADING_H
