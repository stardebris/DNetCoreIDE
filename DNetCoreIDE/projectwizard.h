#ifndef PROJECTWIZARD_H
#define PROJECTWIZARD_H

#include <QMainWindow>

namespace Ui {
class projectWizard;
}

class projectWizard : public QMainWindow
{
    Q_OBJECT

public:
    explicit projectWizard(QWidget *parent = 0);
    QString currentPath;
    ~projectWizard();

private slots:
    void on_browseBtn_clicked();

    void on_cancelBtn_clicked();

    void on_createDirChk_clicked();

    void on_projectNameTxt_textChanged(const QString &arg1);

    void on_createBtn_clicked();

private:
    Ui::projectWizard *ui;
};

#endif // PROJECTWIZARD_H
