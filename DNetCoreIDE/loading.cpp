#include "loading.h"
#include "ui_loading.h"
#include "dnetproject.h"
#include <QDir>
#include <QString>
#include <QTextStream>
#include <QTimer>
#include <QDebug>

Loading::Loading(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Loading)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint);

    this->setFixedSize(this->size());

    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

void Loading::initProject()
{
    ui->statusLabel->setText("Creating project files...");

    DNetProject *project = new DNetProject();

    project->frameworkTarget = projectTargetFramework;
    project->projectName = projectName;
    project->projectLanguage = projectLang;

    QString filename= currentPath+"/"+projectName+".dncproj";

    if (makeDirectory)
    {
        QDir().mkdir(currentPath+"/"+projectName);
        filename= currentPath+"/"+projectName+"/"+projectName+".dncproj";
    }

    ui->progressBar->setValue(ui->progressBar->value() +1);
    ui->statusLabel->setText("Creating source files...");

    QString prog = "";

    if (Get::osName() == "windows")
    {
        if (makeDirectory)
            prog = "cmd.exe /C \"cd " + currentPath + "/" + projectName + " & dotnet new --lang " + projectLang.replace(" (CSharp)", "").replace(" (FSharp)", "") + "\"";
        else
            prog = "cmd.exe /C \"cd " + currentPath + " & dotnet new --lang " + projectLang.replace(" (CSharp)", "").replace(" (FSharp)", "") + "\"";
    }
    else if (Get::osName() == "linux")
    {
        if (makeDirectory)
            prog = "/bin/bash -c \"cd " + currentPath + "/" + projectName + "; dotnet new --lang " + projectLang.replace(" (CSharp)", "").replace(" (FSharp)", "") + "\"";
        else
            prog = "/bin/bash -c \"cd " + currentPath + "; dotnet new --lang " + projectLang.replace(" (CSharp)", "").replace(" (FSharp)", "") + "\"";
    }

    QProcess* process = new QProcess(this);
    process->start(prog);
    process->waitForFinished();
    QString tmp = process->readAll();

    if (initGit)
    {
        ui->progressBar->setValue(ui->progressBar->value() +1);
        ui->statusLabel->setText("Git init...");

        if (Get::osName() == "windows")
        {
            if (makeDirectory)
                prog = "cmd.exe /C \"cd " + currentPath + "/" + projectName + " & git init\"";
            else
                prog = "cmd.exe /C \"cd " + currentPath + " & git init;\"";
        }
        else if (Get::osName() == "linux")
        {
            if (makeDirectory)
                prog = "/bin/bash -c \"cd " + currentPath + "/" + projectName + "; git init\"";
            else
                prog = "/bin/bash -c \"cd " + currentPath + "; git init;\"";
        }


        process = new QProcess(this);
        process->start(prog);
        process->waitForFinished();
        tmp += process->readAll();

        ui->progressBar->setValue(ui->progressBar->value() +1);
        ui->statusLabel->setText("Git tracking all files...");

        if (Get::osName() == "windows")
        {
            if (makeDirectory)
                prog = "cmd.exe /C \"cd " + currentPath + "/" + projectName + " & git add -A\"";
            else
                prog = "cmd.exe /C \"cd " + currentPath + " & git add -A\"";
        }
        else if (Get::osName() == "linux")
        {
            if (makeDirectory)
                prog = "/bin/bash -c \"cd " + currentPath + "/" + projectName + "; git add -A;\"";
            else
                prog = "/bin/bash -c \"cd " + currentPath + "; git add -A;\"";
        }

        process = new QProcess(this);
        process->start(prog);
        process->waitForFinished();
        tmp += process->readAll();
    }
    else
        ui->progressBar->setValue(ui->progressBar->value() +2);


    ui->progressBar->setValue(ui->progressBar->value() +1);
    ui->statusLabel->setText("Updating source files for project type...");

    if (projectType == "ASP.NET Core")
    {
        if (projectLang == "C#")
        {
            //Change the sources in C# to run as ASP.NET Core (https://docs.asp.net/en/latest/getting-started.html)
            QString path;
            QStringList projJson;

            if (makeDirectory)
                path = (currentPath + "/" + projectName + "/project.json");
            else
                path = (currentPath + "/project.json");

            QFile file(path);

            if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            while (!file.atEnd()) {
                QByteArray line = file.readLine();
                projJson.append(line + "\n");
            }

            QFile().copy("./Resources/SourceFiles/Startup.cs.template", currentPath + "/Startup.cs");
            QFile().copy("./Resources/SourceFiles/Startup.cs.template", currentPath + "/Startup.cs");

            project->projectFiles.append("Startup.cs");
        }
        else if (projectLang == "F#")
        {
            //Change the sources in F# to run as ASP.NET Core (http://stackoverflow.com/questions/38275408/asp-net-core-1-0-f-project)
            QString path;
            QStringList projJson;

            if (makeDirectory)
                path = (currentPath + "/" + projectName + "/project.json");
            else
                path = (currentPath + "/project.json");

            QFile file(path);

            if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            while (!file.atEnd()) {
                QByteArray line = file.readLine();
                projJson.append(line + "\n");
            }

        }
    }
    else
    {
        if (projectLang == "C#")
        {
            project->projectFiles.append("Program.cs");
        }
        else if (projectLang == "F#")
        {
             project->projectFiles.append("Program.fs");
        }
    }

    QString json = project->toJsonString();

    QFile file( filename );

    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << json << endl;
    }

    ui->progressBar->setValue(ui->progressBar->value() +1);
    ui->statusLabel->setText("Finished, opening project...");
}

void Loading::loadCompleted()
{
    QTimer::singleShot(1000, this, SLOT(initProject()));
}

Loading::~Loading()
{
    delete ui;
}
